<?php
	error_reporting(0);
	require_once __DIR__ . '/knn/vendor/autoload.php';

	use Fieg\kNN\Dataset;
	use Fieg\kNN\Node;
	use Fieg\kNN\Schema;

    class KNNClassifier {
    	public function getDisease($symptoms) {
    		//connect to Database and collect all symptoms and associated diseases as dataset
    		$cxn = new PDO("mysql: host=localhost; dbname=ehealth", "root","");
	    	$sql = "SELECT symptoms.id, symptoms.name as symptom, disease.disease FROM symptoms, disease WHERE symptoms.disease_id = disease.id AND ( ";

	    	
	    	foreach ($symptoms as $disease_symptoms => $disease_symptom) {
	    		$sql .= "symptoms.name LIKE '%$disease_symptom%' OR ";
	    	}

	    	$sql .= ")";
	    	$sql = str_replace("OR )", ")", $sql);
	    	$qry = $cxn->prepare($sql);
	    	$qry->execute();
	    	
	    	$row = $qry->fetchAll(PDO::FETCH_ASSOC);

	    	


			//setup parameters needed when running KNN guess function
		    $schema = new \Fieg\kNN\Schema();
		    $schema
		        ->addField('symptom')
		    ;

		    $dataset = new \Fieg\kNN\DataSet(3, $schema);

		    //foreach Symptom stored in database add as new Node to KNN dataset
		    foreach ($row as $signs=>$sign) {
		    	$sign['symptom'] = strtolower($sign['symptom']);
		    	$sign['disease'] = strtolower($sign['disease']);

		    	$dataset->add(new Node(array(
		    		'symptom' => $sign['symptom'],
		    		'disease' => $sign['disease'])));
		    }

		    
		    $diseases = array();
		    //for each disease guess the disease associated and most common diseases as array
	    	foreach ($symptoms as $symptoms=>$symptom) {
	    		$disease = $dataset->guess(new Node(array('symptom' => $symptom)), 'disease');
	    		if (!in_array($disease, $diseases)) {
	    			$diseases[] = $disease;
	    		}
	    	}

	    	return $diseases;
	    }
	}
