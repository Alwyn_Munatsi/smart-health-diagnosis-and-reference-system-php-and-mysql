<?php
	error_reporting(1);
	require_once __DIR__ . '/../vendor/autoload.php';

	use Fieg\kNN\Dataset;
	use Fieg\kNN\Node;
	use Fieg\kNN\Schema;

    $cxn = new PDO("mysql: host=localhost; dbname=ehealth", "root","");
    $sql = "SELECT symptoms.id, symptoms.name as symptom, disease.disease FROM symptoms, disease WHERE symptoms.disease_id = disease.id";
    $qry = $cxn->prepare($sql);
    $qry->execute();
    $row = $qry->fetchAll(PDO::FETCH_ASSOC);


    $schema = new \Fieg\kNN\Schema();
        $schema
            ->addField('symptom')
            ->addField('disease')
        ;

        $dataset = new \Fieg\kNN\DataSet(3, $schema);

        foreach ($row as $symptoms=>$symptom) {
        	$symptom['symptom'] = strtolower($symptom['symptom']);
        	$symptom['disease'] = strtolower($symptom['disease']);

        	$dataset->add(new Node(array(
        		'symptom' => $symptom['symptom'],
        		'disease' => $symptom['disease'])));
        }

        echo "<pre>";
        print_r($dataset);
        echo "</pre>";

        echo $dataset->guess(new Node(array('symptom' => 'itchy rash')), 'disease');